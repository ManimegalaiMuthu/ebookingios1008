//
//  UIWidgetsExtension
//  ShareEbooking
//
//  Created by Admin on 8/25/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
  func setBottomBorder() {
    //self.borderStyle = .none
    self.layer.backgroundColor = UIColor.white.cgColor
    self.layer.masksToBounds = false
    self.layer.shadowColor = UIColor.gray.cgColor
    self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
    self.layer.shadowOpacity = 1.0
    self.layer.shadowRadius = 0.0
  }
    
 func createFloatingActionButton() {
        layer.backgroundColor = UIColor.red.cgColor
        layer.cornerRadius = frame.height / 2
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0
            , height: 10)
        
    }
func setBordersSettings() {
       
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
        self.layer.borderColor = UIColor.red.cgColor
        //self.setTitleColor(c1GreenColor, for: .normal)
        self.layer.masksToBounds = true
    }
}
extension UIView {
func setBottomLine() {
  //self.borderStyle = .none
  self.layer.backgroundColor = UIColor.white.cgColor
  self.layer.masksToBounds = false
  self.layer.shadowColor = UIColor.gray.cgColor
  self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
  self.layer.shadowOpacity = 1.0
  self.layer.shadowRadius = 0.0
    }
    
}
extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: size.height - lineWidth, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
extension UINavigationController {
    func setCustomNavigation(view: UIViewController) {
  //self.borderStyle = .none
   // Navigation Bar:
        view.navigationController?.navigationBar.barTintColor = UIColor.systemYellow

        // Navigation Bar Text:
        view.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.red]

        // Tab Bar:
        view.tabBarController?.tabBar.barTintColor = UIColor.blue

        // Tab Bar Text:
        view.tabBarController?.tabBar.tintColor = UIColor.red
    }
}

@IBDesignable
public class Button: UIButton {
    @IBInspectable public var borderColor:UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable public var borderWidth:CGFloat = 0 {
        didSet {
        layer.borderWidth = borderWidth
        }
    }
    @IBInspectable public var cornerRadius:CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}

class CustomTabBar : UITabBar {
    @IBInspectable var height: CGFloat = 0.0

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        if height > 0.0 {
            sizeThatFits.height = height
        }
        return sizeThatFits
    }
    
}

extension UIButton {
    func isError(baseColor: CGColor, numberOfShakes shakes: Float, revert: Bool) {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "shadowColor")
        animation.fromValue = baseColor
        animation.toValue = UIColor.red.cgColor
        animation.duration = 0.4
        if revert { animation.autoreverses = true } else { animation.autoreverses = false }
        self.layer.add(animation, forKey: "")

        let shake: CABasicAnimation = CABasicAnimation(keyPath: "position")
        shake.duration = 0.07
        shake.repeatCount = shakes
        if revert { shake.autoreverses = true  } else { shake.autoreverses = false }
        shake.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 30, y: self.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 30, y: self.center.y))
        self.layer.add(shake, forKey: "position")
    }
}
