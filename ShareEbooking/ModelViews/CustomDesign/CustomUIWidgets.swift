//
//  CustomUIWidgets.swift
//  TestApp
//
//  Created by Admin on 8/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit
public class CustomUIWidgets {
    
    let transparentView = UIView()
    var tableView = UITableView()
    var view = UIView()
   
    var frames: CGRect!
    let keyWindow = UIApplication.shared.connectedScenes
    .filter({$0.activationState == .foregroundActive})
    .map({$0 as? UIWindowScene})
    .compactMap({$0})
    .first?.windows
    .filter({$0.isKeyWindow}).first
    
    let marginStart:CGFloat = 10.0
    let spacing:CGFloat = 20.0
    
    init(tableView: UITableView,view: UIView){
        self.tableView = tableView
        self.view = view
       
    }
    init(){}
    
    @objc func removeDropDown() {
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.tableView.frame = CGRect(x: self.frames.origin.x + self.marginStart, y: self.frames.origin.y + (self.frames.height), width: self.frames.width -  self.spacing, height: 0)
        }, completion: nil)
    }
    
    func showDropDown(viewFrame: CGRect,dataSource: [String]) {
        
        frames = viewFrame
        let window = keyWindow
        transparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        
        tableView.frame = CGRect(x: frames.origin.x + marginStart, y: frames.origin.y + (frames.height ), width: frames.width - spacing , height: 0)
        self.view.addSubview(tableView)
        tableView.layer.cornerRadius = 5
        
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        tableView.reloadData()
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(removeDropDown))
        transparentView.addGestureRecognizer(tapgesture)
        transparentView.alpha = 0
    
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.tableView.frame = CGRect(x: self.frames.origin.x + self.marginStart, y: self.frames.origin.y + (self.frames.height ) , width: self.frames.width -  self.spacing, height: CGFloat(dataSource.count * 50))
            
        }, completion: nil)
    }
    
    func reQuiredInputLabel(value: [String], label:[UILabel] ){
        var count = 0
        for lbl in label{
            let passwordAttriburedString = NSMutableAttributedString(string: value[count])
            let asterix = NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red])
               passwordAttriburedString.append(asterix)
            lbl.attributedText = passwordAttriburedString
             count+=1
        }
    }
    
    func repositionTabBarAtTheTop(tabBar: UITabBar,view: UIView){
        var tabFrame:CGRect = tabBar.frame
        tabFrame.origin.y = view.frame.origin.y
        tabBar.frame = tabFrame
    }
    
    func addBottomLine(BTNs:[UIButton]){
        for btn in BTNs{
            btn.setBottomBorder()
        }
        
        
    }
    
}

class ButtonWithImage: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
    }
}
