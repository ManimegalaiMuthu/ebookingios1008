//
//  AllBookingsTableViewCell.swift
//  ShareEbooking
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AllBookingsTableViewCell: UITableViewCell {

    @IBOutlet weak var IV_DOT: UIImageView!
    @IBOutlet var LBL_ID: UILabel!
     @IBOutlet var LBL_STATUS: UILabel!
     @IBOutlet var LBL_VEHICLEID_AND_NAME: UILabel!
     @IBOutlet var LBL_TIME: UILabel!
     @IBOutlet var LBL_PRODUCT_VALUE: UILabel!
     @IBOutlet var BTN_SHOW_INFO: UIButton!
    
    var actionBlock: (() -> Void)? = nil
    func updateTable(bookings: ResponseData){
        LBL_ID.text = bookings.BookingDetailsID
        LBL_STATUS.text = bookings.StatusDisplay
        LBL_VEHICLEID_AND_NAME.text =
            bookings.VehicleNumber + "(" + bookings.ConsumerName + ")"
        LBL_TIME.text = bookings.BookedFrom + " - " + bookings.BookedTo
        LBL_PRODUCT_VALUE.text = bookings.ProductName
        
//        switch bookings.Status{
//        case Constants.PENDING_WORKSHOP_STATUS:
//            break
//        case Constants.PENDING_CUSTOMER_STATUS:
//            break
//        case Constants.CONFIRMED_STATUS:
//            break
//        case Constants.CANCELLED_STATUS:
//            break
//        case Constants.DECLINED_STATUS:
//            break
//        case Constants.IN_PROGRESS_STATUS:
//            break
//        case Constants.COMPLETED_STATUS:
//            break
//        case Constants.NO_SHOW_STATUS:
//            break
//
//
//        }
    }
   
    func selectedRow(){
        print("click click  \(LBL_ID.text ?? "empty--")")
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func OnClickShowBookingInfo(_ sender: Any) {
         actionBlock?()
    }
    
    
    
}
