//
//  CalendarViewTableViewCell.swift
//  ShareEbooking
//
//  Created by Admin on 10/12/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class CalendarViewTableViewCell: UITableViewCell {

    @IBOutlet weak var LBL_NAME: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    @IBOutlet weak var DOT: UIImageView!
    
    let apiRequest = APIRequest()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
       func updateTable(bookings: ResponseData){
        LBL_TIME.text = bookings.BookedTo
           LBL_NAME.text = bookings.VehicleNumber + "(" + bookings.ConsumerName + ")"
        }
      
       func selectedRow(){
           
       }
       
    
}
