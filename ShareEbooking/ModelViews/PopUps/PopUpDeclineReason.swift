//
//  PopUpDeclineReason.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

class PopUpDeclineReason: UIView{
    static let instance = PopUpDeclineReason()
    var navigationController = UINavigationController()
    var bookingID:String?
    var apiRequest = APIRequest()
    var bookingData : ResponseData!
    var reason: String = ""
    
    @IBOutlet var PARENT_VIEW: UIView!
    @IBOutlet weak var TXT_REMARKS: UITextField!
    
    @IBOutlet weak var CB_UNAVAILABLE: CustomCheckBox!
    @IBOutlet weak var CB_OUT_OF_STOCK: CustomCheckBox!
    @IBOutlet weak var CB_OTHER_REASON: CustomCheckBox!
    
    override init(frame: CGRect)  {
        super.init(frame:frame)
       
    }
   
    @IBAction func CBUnAvailableChecked(_ sender: Any) {
        if(CB_UNAVAILABLE.isChecked){
            CB_OUT_OF_STOCK.isChecked = false
            CB_OTHER_REASON.isChecked = false
        }
    }
    
    @IBAction func CBOtherChecked(_ sender: Any) {
        if(CB_OTHER_REASON.isChecked){
            CB_OUT_OF_STOCK.isChecked = false
            CB_UNAVAILABLE.isChecked = false
        }
    }
    @IBAction func CBOutOfStockChecked(_ sender: Any) {
        if(CB_OUT_OF_STOCK.isChecked){
            CB_OTHER_REASON.isChecked = false
            CB_UNAVAILABLE.isChecked = false
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showPopUp(bookingID:String,nav: UINavigationController){
        self.navigationController = nav
        self.bookingID = bookingID
        Bundle.main.loadNibNamed(EbookingConstants.PopUpIdentifier.POP_DECLINE_REASON, owner: self, options: nil)
        PARENT_VIEW.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        PARENT_VIEW.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(PARENT_VIEW)
        CB_OTHER_REASON.isChecked = true
     
    }
    
    @IBAction func BtnCancel(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
    }
    
    @IBAction func BtnSubmit(_ sender: Any) {
        if(CB_UNAVAILABLE.isChecked){
            reason = EbookingConstants.UNAVAILABLE_SLOT
        } else if(CB_OUT_OF_STOCK.isChecked){
            reason = EbookingConstants.OUT_OF_STOCK
        }else{
            // Other reaason
            reason = "Others"
        }
        
        submitDeclineBooking()
    }
    
    private func submitDeclineBooking(){
        let remarks = TXT_REMARKS.text ?? ""
        let param = ["BookingDetailsID":self.bookingID!,
                     "BookingStatusID":EbookingConstants.DECLINED_STATUS,
                      "Reason":reason,
                      "Remarks":remarks] as [String : Any]?
        EbookingUtils.showLoading(view: self)
        apiRequest.request(url:URL(string: EbookingConstants.Api.updateBookingStatus)!,parameters: param!){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                print("DECLINE BOOKING json ---",json!)
                    self.PARENT_VIEW.removeFromSuperview()
                    PopUpBookingUpdate.instance.showPopUp(message: responseMessage.message, nav: self.navigationController)
                }else{
                    // show popup instead
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self)
                }
            EbookingUtils.stopLoading(view: self)
            }
        
    }
}

