//
//  PopUpDeclineModify.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

class PopUpDeclineModify: UIView{
    
    static let instance = PopUpDeclineModify()
    @IBOutlet weak var PARENT_VIEW: UIView!
    var navigationController = UINavigationController()
    var bookingData : ResponseData!
    override init(frame: CGRect){
        super.init(frame:frame)
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showPopUp(nav: UINavigationController,bookingResponse:ResponseData){
        self.bookingData = bookingResponse
        self.navigationController = nav
        Bundle.main.loadNibNamed(EbookingConstants.PopUpIdentifier.POP_DECLINE_MODIFY_BOOKING, owner: self, options: nil)
        PARENT_VIEW.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        PARENT_VIEW.autoresizingMask = [.flexibleHeight,.flexibleWidth]

        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(PARENT_VIEW)
    }
    @IBAction func BtnDecline(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
        PopUpDeclineReason.instance.showPopUp(bookingID: self.bookingData.BookingDetailsID, nav: self.navigationController)
    }
    
    @IBAction func BtnModify(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
        let storyBoard: UIStoryboard = UIStoryboard(name: EbookingConstants.StoryBoards.BOOKING_STATUS, bundle: nil)
        let vController = storyBoard.instantiateViewController(withIdentifier: EbookingConstants.VControllerIdentifier.UPDATE_BOOKING_ID) as! UpdateBookingInfoViewController
        vController.bookingData = bookingData
           navigationController.pushViewController(vController, animated: true)
    }
   
}

