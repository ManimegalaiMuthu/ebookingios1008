//
//  PopUpBookingUpdate.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

class PopUpBookingUpdate: UIView{
    
    static let instance = PopUpBookingUpdate()
    @IBOutlet weak var PARENT_VIEW: UIView!
    @IBOutlet weak var LBL_MESSAGE: UILabel!
    
    var navigationController = UINavigationController()
    
    override init(frame: CGRect)  {
        super.init(frame:frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showPopUp(message:String,nav: UINavigationController){
        self.navigationController = nav
        
        Bundle.main.loadNibNamed(EbookingConstants.PopUpIdentifier.POP_COMMON, owner: self, options: nil)
        PARENT_VIEW.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        PARENT_VIEW.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        LBL_MESSAGE.text = message
        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(PARENT_VIEW)
    }
    
    @IBAction func BtnOk(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
        self.navigationController.popToRootViewController(animated: true)
    }
    
}

