//
//  StatusObject.swift
//  ShareEbooking
//
//  Created by Admin on 10/12/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class StatusObject{
    var id:Int?
    var statusDescription:String?
    init(id: Int, statusDescription:String) {
        self.id = id
        self.statusDescription = statusDescription
    }
}
