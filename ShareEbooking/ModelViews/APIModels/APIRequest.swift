//
//  APIRequest.swift
//  ShareEbooking
//
//  Created by Admin on 10/11/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public enum ResponseCode:Int {
  case Success = 5000
  case ForceUpdate = 5001
  case UpdateAvailable = 5002
  case Unknown = 0
  case Error = 2000
  case Cancelled = 999
  case SessionExpired = 9999
//  case StockUpdateFailure = 7001
//  case SubmitInvoiceFailure = 1003
}

class APIRequest{
 
    // URL(string: EbookingConstants.Api.fetchBookingUrl)!
    func request(url:URL,parameters:Parameters = [:],completionHandler: @escaping (_ responseCode:ResponseCode,_ responseMessage:ResponseMessage,_ json:JSON?,_ response:AFDataResponse<Any>)->Void) {
        
        AF.requestWithoutCache(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: EbookingConstants.Headers.apiHeader()).responseJSON() {
            (response) in
            
           if let error = response.error {
                    if (error as NSError?)?.code == NSURLErrorCancelled {
                        completionHandler(ResponseCode.Cancelled,ResponseMessage(),nil,response)
                    } else if response.response?.statusCode == 403 {   //Forbidden(403) expires session
                      completionHandler(ResponseCode.SessionExpired,ResponseMessage(),nil,response)
                    } else {
                      completionHandler(ResponseCode.Unknown,ResponseMessage(),nil,response)
                    }
                    return
                  }
            if let data = response.data {
                
                
              if let json = try? JSON(data: data) {
                
                var responseMessage:ResponseMessage = ResponseMessage(json: json["ResponseMessage"])
                if responseMessage.header == "" && responseMessage.message == "" {
                  responseMessage = ResponseMessage()
                }
                completionHandler(ResponseCode(rawValue: json["ResponseCode"].intValue) ?? .Unknown, responseMessage, json,response)
                return
              }
            }
            completionHandler(.Unknown, ResponseMessage(), nil,response)
            
        }
        
        
    }
    
}
