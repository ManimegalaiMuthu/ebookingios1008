//
//  BaseWebServiceManager.swift
//  ersgProjectBase
//
//  Created by Ben on 6/12/18.
//  Copyright © 2018 Ben. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// This response code is one of the fields returned from the API, it is NOT equal to the HTTP response code. To check for HTTP response code use response.statusCode
//public enum ResponseCode:Int {
//  case Success = 5000
//  case ForceUpdate = 5001
//  case UpdateAvailable = 5002
//  case Unknown = 0
//  case Error = 2000
//  case Cancelled = 999
//  case SessionExpired = 9999
////  case StockUpdateFailure = 7001
////  case SubmitInvoiceFailure = 1003
//}

// This will contain information used to display a textual response to the user if required. The request method of BaseWebServiceManager will create an instance of this with the data from the API call, the init methods of this class is not expected to be used within the app itself.
public class ResponseMessage {
  public let header:String
  public let message:String
  public let btnText1:String
  public let btnText2:String
  
  public init(json:JSON) {
    
    header = json["Header"].stringValue
    message = json["Message"].stringValue
    btnText1 = json["BtnText1"].stringValue
    btnText2 = json["BtnText2"].stringValue
  }
  
  public init(header:String = "", message:String = "", btnText1:String = "", btnText2:String = "") {
    
    self.header = header
    self.message = message
    self.btnText1 = btnText1
    self.btnText2 = btnText2
  }
}

/**
 This is a class used to perform API calls. It is designed to be subclassed into a singleton, if not an instance of BaseWebServiceManager is required to perform it's related actions
 */
open class BaseWebServiceManager {
  
  public init() {}
  
  /**
   Queue for requests
   */
  public var operationQueue:OperationQueue = OperationQueue()
  
  /**
   This method is used for API calls. Each request will be added to the operationQueue of BaseWebServiceManager singleton
   
   - Parameters:
      - url:URLConvertible - uses URLConvertible of Alamofire, if nothing accepts URL of Foundation
      - method:HTTPMethod - defaults to POST
      - parameters:Parameters - Dictionary to be sent with API call as HTTPBody
      - encoding:ParameterEncoding - defaults to JSONEncoding.default, which is used for most API. Need not provide unless required
      - headers:HTTPHeaders - Dictionary to be sent with API call as HTTPHeaders
      - completionHandler: Method to be called once API call returns success or failure
      - responseCode:ResponseCode - response code of API Not HTTP, refer to enum ResponseCode above for information
      - responseMessage:ResponseMessage - used to display textual response, refer to class ResponseMessage above for information
      - json:JSON - optional JSON object from SwiftyJSON which contains the data from the API, nil if no data is received.
   - returns:
   BlockOperation: The request will be made into a BlockOperation which will be queued into the operationQueue of BaseWebServiceManager automatically, the returned reference is meant for keeping reference only.
   */
  @discardableResult
  public func request(_ url:URLConvertible,
                             method:HTTPMethod = .post,
                             parameters:Parameters = [:],
                             encoding:ParameterEncoding = JSONEncoding.default,
                             headers:HTTPHeaders = EbookingConstants.Headers.PostLogin(),
                             completionHandler: @escaping (_ responseCode:ResponseCode, _ responseMessage:ResponseMessage, _ json:JSON?)->Void = { _, _, _ in }) -> BlockOperation {
    return requestWithResponse(url, method: method, parameters: parameters, encoding: encoding, headers: headers) { responseCode, responseMessage, json, _ in
      completionHandler(responseCode, responseMessage, json)
    }
  }
  
  /**
   This method is used for API calls. Each request will be added to the operationQueue of BaseWebServiceManager singleton
   
   - Parameters:
      - url:URLConvertible - uses URLConvertible of Alamofire, if nothing accepts URL of Foundation
      - method:HTTPMethod - defaults to POST
      - parameters:Parameters - Dictionary to be sent with API call as HTTPBody
      - encoding:ParameterEncoding - defaults to JSONEncoding.default, which is used for most API. Need not provide unless required
      - headers:HTTPHeaders - Dictionary to be sent with API call as HTTPHeaders
      - completionHandler: Method to be called once API call returns success or failure
      - responseCode:ResponseCode - response code of API Not HTTP, refer to enum ResponseCode above for information
      - responseMessage:ResponseMessage - used to display textual response, refer to class ResponseMessage above for information
      - json:JSON - optional JSON object from SwiftyJSON which contains the data from the API, nil if no data is received.
      - response:DataResponse<Any> - full DataResponse, can be used to obtained information not found from other objects of this method.
   - returns:
   BlockOperation: The request will be made into a BlockOperation which will be queued into the operationQueue of BaseWebServiceManager automatically, the returned reference is meant for keeping reference only.
   */
  @discardableResult
  public func requestWithResponse(_ url:URLConvertible,
                                         method:HTTPMethod = .post,
                                         parameters:Parameters = [:],
                                         encoding:ParameterEncoding = JSONEncoding.default,
                                         headers:HTTPHeaders = EbookingConstants.Headers.PostLogin(),
                                         completionHandler: @escaping (_ responseCode:ResponseCode, _ responseMessage:ResponseMessage, _ json:JSON?, _ response:AFDataResponse<Any>)->Void = { _, _, _, _ in }) -> BlockOperation {
    
    let operation = BlockOperation {
      Alamofire.Session.default.requestWithoutCache(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON() { response in
        
        if let error = response.error {
          if (error as NSError?)?.code == NSURLErrorCancelled {
            completionHandler(.Cancelled, ResponseMessage(), nil, response)
          } else if response.response?.statusCode == 403 {   //Forbidden(403) expires session
            completionHandler(.SessionExpired, ResponseMessage(), nil, response)
          } else {
            completionHandler(.Unknown, ResponseMessage(), nil, response)
          }
          return
        }
        
        if let data = response.data {
          if let json = try? JSON(data: data) {
            var responseMessage:ResponseMessage = ResponseMessage(json: json["ResponseMessage"])
            if responseMessage.header == "" && responseMessage.message == "" {
              responseMessage = ResponseMessage()
            }
            completionHandler(ResponseCode(rawValue: json["ResponseCode"].intValue) ?? .Unknown, responseMessage, json, response)
            return
          }
        }
        completionHandler(.Unknown, ResponseMessage(), nil, response)
      }
    }
    operation.qualityOfService = .userInitiated
    operation.queuePriority = .veryHigh
    operationQueue.addOperation(operation)
    return operation
  }
}
