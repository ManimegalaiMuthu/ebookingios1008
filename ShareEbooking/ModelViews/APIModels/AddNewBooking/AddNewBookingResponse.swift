//
//  AddNewBookingResponse.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
struct AddNewBookingAPIResponse:Decodable{
   var Data:String?
   var Error:String?
   var Msg:String?
   var ResponseCode:Int?
   var ResponseMessage:Message
}
