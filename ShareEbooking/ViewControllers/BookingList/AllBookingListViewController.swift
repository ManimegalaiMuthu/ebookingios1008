//
//  AllBookingListViewController.swift
//  ShareEbooking
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import iOSDropDown
import SwiftyJSON
import Alamofire

class AllBookingListViewController: UIViewController, UITableViewDelegate,UITableViewDataSource{

    let apiRequest = APIRequest()
    var objectsArray = [String]()
    @IBOutlet weak var DROP_DOWN_STATUS: DropDown!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var FButton: UIButton!
    @IBOutlet weak var SB_KEYWORD: UISearchBar!
    @IBOutlet var RefreshBookingList: [UITableView]!
    var selectedStatusID: Int? = nil
    var refreshList: UIRefreshControl?
    
    var bookingData = [ResponseData](){
        didSet{
            DispatchQueue.main.async {
                 self.tableView.reloadData()
            }
        }
    }
    var filteredBookings : [ResponseData]!
    
    let cellClassNameIdentifier = "AllBookingsTableViewCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        FButton.createFloatingActionButton()
       
         let nib = UINib(nibName: cellClassNameIdentifier, bundle: nil)
               tableView.register(nib, forCellReuseIdentifier:cellClassNameIdentifier)
               tableView.delegate = self
               tableView.dataSource = self
        SB_KEYWORD.delegate = self
        filteredBookings = bookingData
        SB_KEYWORD.searchTextField.backgroundColor = .clear
        let objects = EbookingUtils.statusObjects()
        objectsArray = EbookingUtils.getStatusDescription(object: objects)
        DROP_DOWN_STATUS.optionArray = objectsArray
        DROP_DOWN_STATUS.optionIds = EbookingUtils.getStatusID(object: objects)
        DROP_DOWN_STATUS.didSelect{(selectedText , index ,id) in
            self.selectedStatusID = id
            EbookingUtils.addPadding(textField: self.DROP_DOWN_STATUS)
            EbookingUtils.showLoading(view: self.view)
            self.getBookingList(param: ["status":self.selectedStatusID!])
        }
        EbookingUtils.showLoading(view: self.view)
        getBookingList(param: [:])
        addRefreshingList()
    }
    
    
    
    func addRefreshingList(){
        refreshList = UIRefreshControl()
        refreshList?.tintColor = UIColor.red
        refreshList?.addTarget(self, action: #selector(pullDownToRefreshList), for: .valueChanged)
        tableView.addSubview(refreshList!)
    }
    @objc func pullDownToRefreshList(){
        if(self.selectedStatusID == nil){
            getBookingList(param: [:])
        }else{
            getBookingList(param: ["status":self.selectedStatusID!])
        }
    }
    
    @IBAction func OnClickAddNewBooking(_ sender: Any) {
        
        //Instance of the second screen
        let storyBoard: UIStoryboard = UIStoryboard(name: EbookingConstants.StoryBoards.ADD_NEW_BOOKING_STORYBOARD, bundle: nil)
        let vController = storyBoard.instantiateViewController(withIdentifier: EbookingConstants.VControllerIdentifier.ADD_NEW_BOOKING_ID) as! AddNewBookingController
        self.navigationController?.pushViewController(vController, animated: true)
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                // let dataSource = source[section]
                 return filteredBookings.count
            }
    
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: cellClassNameIdentifier,for: indexPath) as! AllBookingsTableViewCell
                let data = filteredBookings[indexPath.row]
                
                cell.updateTable(bookings: data)
                cell.actionBlock = {
                    //show contoller by booking status
                    let storyBoard: UIStoryboard = UIStoryboard(name: EbookingConstants.StoryBoards.BOOKING_STATUS, bundle: nil)
                    let vController = EbookingUtils.getViewController(status: data.Status,storyBoard: storyBoard, data: data)
                    //vController.bookingData =
                self.navigationController?.pushViewController(vController, animated: true)
                }
               
                return cell
            }

    func getBookingList(param:Parameters = [:]){
    apiRequest.request(url:URL(string: EbookingConstants.Api.fetchBookingUrl)!,parameters: param){responseCode,responseMessage,json,response in
        
        let message = responseMessage.message
            if(responseCode == .Success){
                let decoder = JSONDecoder()
                do{
                    let responseData = try decoder.decode(APIResponse.self, from: response.data!)
                    let data = responseData.Data
                     self.bookingData = data
                     self.filteredBookings = data
                     self.tableView.reloadData()
                 }catch{
                    self.bookingData = []
                    self.filteredBookings = []
                    self.tableView.reloadData()
                    EbookingUtils.displayToastMessage(EbookingConstants.NO_RESULT_FOUND,view: self.view)
                }
               
            }else{
               self.bookingData = []
               self.filteredBookings = []
               self.tableView.reloadData()
                print("myBOOKINGS ----- 2 ---",message)
               // show popup here
            }
        
        if(self.refreshList != nil && ((self.refreshList?.isRefreshing) != nil)){
            self.refreshList?.endRefreshing()
        }
        EbookingUtils.stopLoading(view: self.view)
            
        }
    }
}

extension AllBookingListViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredBookings = []
        
        if(searchText.isEmpty){
            filteredBookings = bookingData
        }else{
            for booking in bookingData{
                if(booking.ConsumerName.lowercased().contains(searchText.lowercased())){
                    filteredBookings.append(booking)
                }
                
            }
        }
        
        self.tableView.reloadData()
    }
}
