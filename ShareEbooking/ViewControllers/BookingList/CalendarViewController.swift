//
//  CalendarViewController.swift
//  ShareEbooking
//
//  Created by Admin on 8/24/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import FSCalendar
import Alamofire
import SwiftyJSON

class CalendarViewController: UIViewController, FSCalendarDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var FloatingBTN: UIButton!
    @IBOutlet weak var Calendar: FSCalendar!
    @IBOutlet weak var LBL_SELECTED_DATE: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var refreshList: UIRefreshControl?
    let apiRequest = APIRequest()
    var stringDate:String = ""
    
     let cellClassNameIdentifier = "CalendarViewTableViewCell"
    
    var bookingDetails = [ResponseData](){
           didSet{
               DispatchQueue.main.async {
                    self.tableView.reloadData()
               }
           }
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    FloatingBTN.createFloatingActionButton()
    Calendar.delegate = self
        
        let nib = UINib(nibName: cellClassNameIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier:cellClassNameIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        
        let currentDate = EbookingUtils.getCurrentDate(date: Date())
        LBL_SELECTED_DATE.text = currentDate
        
        EbookingUtils.showLoading(view: self.view)
        getBookingList(param: [:])
        addRefreshingList()
    }
    
    
    func addRefreshingList(){
        refreshList = UIRefreshControl()
        refreshList?.tintColor = UIColor.red
        refreshList?.addTarget(self, action: #selector(pullDownToRefreshList), for: .valueChanged)
        tableView.addSubview(refreshList!)
    }
    @objc func pullDownToRefreshList(){
        if(self.stringDate == ""){
            getBookingList(param: [:])
        }else{
            getBookingList(param: ["BookingDate":self.stringDate])
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                   // let dataSource = source[section]
                    return bookingDetails.count
               }
               func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                   let cell = tableView.dequeueReusableCell(withIdentifier: cellClassNameIdentifier,for: indexPath) as! CalendarViewTableViewCell
                   
                 // let productLine = source[indexPath.section]
                   let data = bookingDetails[indexPath.row]
                   
                   cell.updateTable(bookings: data)
                   return cell
               }


    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "EEEE, dd MMMM, yyyy"
        stringDate = dateFormater.string(from: date)
        LBL_SELECTED_DATE.text = stringDate
        dateFormater.dateFormat = "dd/M/yyyy"
        stringDate = dateFormater.string(from: date)
         let param = ["BookingDate": stringDate]
        
        EbookingUtils.showLoading(view: self.view)
        getBookingList(param: param)
    }
    
    func getBookingList(param:Parameters = [:]){
         //  let param = ["BookingDate": date]
        apiRequest.request(url:URL(string: EbookingConstants.Api.fetchBookingUrl)!,parameters:param){responseCode,responseMessage,json,response in
                          if(responseCode == .Success){
                   let decoder = JSONDecoder()
                   do{
                       let responseData = try decoder.decode(APIResponse.self, from: response.data!)
                       let data = responseData.Data
                        self.bookingDetails = data
                       self.tableView.reloadData()
                    }catch{
                        print("RESPONSE---->>>>", "Table error-----",error)
                   }
               }
            if(self.refreshList != nil && ((self.refreshList?.isRefreshing) != nil)){
                self.refreshList?.endRefreshing()
            }
            EbookingUtils.stopLoading(view: self.view)
           }
       }

    @IBAction func OnClickAddNewBooking(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "AddNewBookingController", bundle: nil)
        let vController = storyBoard.instantiateViewController(withIdentifier: "Add_New_Booking") as! AddNewBookingController
        self.navigationController?.pushViewController(vController, animated: true)
        
    }
}
