//
//  UpdateBookingInfoViewController.swift
//  ShareEbooking
//
//  Created by Admin on 8/28/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import iOSDropDown

class UpdateBookingInfoViewController: UIViewController {
    var id: String = ""
    var vehicleAndUser: String = ""
    var dateTime: String = ""
    var serviceType: String = ""
    var product: String = ""
    
    let servicesDropDown = ["Service 1","Service 2","Service 3"]
    let oilChangeDropDown = ["Oilchange 1","OilChange 2","OilChange 3"]
    let dateDropDown = ["date 1","dtae 2","date 3"]
    let timeDropDown = ["time 1","time 2","time 3"]
  //  var pendingBooking: EBookingInfo?
    
    let customUI = CustomUIWidgets()
    let ebookingUtils = EbookingUtils()
    // chevron.compact.left
    
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    @IBOutlet weak var LBL_BOOKING_STATUS: UILabel!
    @IBOutlet weak var LBL_VEHICLE_AND_NAME: UILabel!
    
    @IBOutlet weak var LBL_SERVICES: UILabel!
    @IBOutlet weak var LBL_OIL_CHANGE: UILabel!
    @IBOutlet weak var LBL_DATE: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    
    @IBOutlet weak var SERVICES_VIEW: UIView!
    @IBOutlet weak var OILCHANGE_VIEW: UIView!
    @IBOutlet weak var DATE_VIEW: UIView!
    @IBOutlet weak var TIME_VIEW: UIView!
    
    @IBOutlet weak var SERVICE_TYPE: DropDown!
    @IBOutlet weak var OIL_CHANGE: DropDown!
    @IBOutlet weak var DATE: UIButton!
    @IBOutlet weak var TIME: UIButton!
    
    var apiRequest = APIRequest()
    var productsArray = [String]()
    var serviceArray = [String]()
    var ruleMatrixData:GetRuleMatrixAPIResponse?
    
    var selectedProduct:String?
    var selectedProductID:Int?
    var newBookingID:String?
    var bookingMessage:String?
    
    var bookingMatrixID :Int?
    var bookingServiceTypeID :Int?
    var serviceTypeDurationHour :Int?
    var serviceTypeDescription :String?
    var serviceTypeDescriptionValue :String?
    var bookingServiceModeID :Int?
    var bookingModeDescriptionValue :String?
    var iSRequiredPickup :Bool?
    var maxOpsCount :Int?
    var opsPanaltyDays :Int?
    var maxEditableCount :Int?
    var bookingData : ResponseData!
    var btnInputs = [UIButton]()
    var textFields = [UITextField]()
    
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = EbookingConstants.EDIT_BOOKING
        setUpRequiredInputLabels()
        LBL_BOOKING_ID.text = bookingData.BookingDetailsID
        LBL_BOOKING_STATUS.text = bookingData.StatusDisplay
        LBL_VEHICLE_AND_NAME.text = bookingData.VehicleNumber +
            " (\(bookingData.ConsumerName))"
        getProducts()
    }
    
    @IBAction func BtnCall(_ sender: Any) {
        
        mobileNumber = "07768988959"
        let url:NSURL = URL(string: "TEL://\(mobileNumber)")! as NSURL
        UIApplication.shared.open(url as URL, options:[:], completionHandler: nil)

        
    }
    func forUpadteBookingInfo(bookingData : ResponseData){
        self.bookingData = bookingData
    }
    
    func getProducts(){
        EbookingUtils.showLoading(view: self.view)
        let sampleTradeId = "T170000130"
        apiRequest.request(url:URL(string: EbookingConstants.Api.getProducts)!,parameters: ["TradeID":sampleTradeId]){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(GetProductsAPIResponse.self, from: response.data!)
                        let data = responseData.Data
                        let objects = data
                        self.productsArray = ShellProducts.getProductName(object: objects)
                        self.OIL_CHANGE.optionArray = self.productsArray
                        self.OIL_CHANGE.optionIds = ShellProducts.getProductID(object: objects)
                        self.getRulematrix()
                     }catch{
                        EbookingUtils.stopLoading(view: self.view)
                        EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_GET_PRODUCTS,view: self.view)
                    }
                }else{
                    EbookingUtils.stopLoading(view: self.view)
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            }
        }
    
    func getRulematrix(){
        apiRequest = APIRequest()
        apiRequest.request(url:URL(string: EbookingConstants.Api.getRuleMatrix)!,parameters: [:]){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(GetRuleMatrixAPIResponse.self, from: response.data!)
                        self.ruleMatrixData = responseData
                        self.serviceArray = RuleMatrix.getServiceTypeDescriptionValue(object: responseData)
                        self.SERVICE_TYPE.optionArray = self.serviceArray
                        self.initServiceTypeDropDown()
                        self.initProductsDropDown()
                     }catch{
                        EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_GET_SERVICES,view: self.view)
                    }
                }else{
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
        
    }

    @IBAction func showServiceDate(_ sender: AnyObject?) {
        ebookingUtils.showDatePicker(view: self.view, btn: DATE)
     }
    @IBAction func showServiceTime(_ sender: AnyObject?) {
        ebookingUtils.showTimePicker(view: self.view, btn: TIME)
    }
    
    func initServiceTypeDropDown(){
         SERVICE_TYPE.didSelect{(selectedText , index ,id) in
            self.bookingMatrixID = self.ruleMatrixData?.Data[index].BookingMatrixID
            self.bookingServiceTypeID = self.ruleMatrixData?.Data[index].BookingServiceTypeID
            self.serviceTypeDurationHour = self.ruleMatrixData?.Data[index].ServiceTypeDurationHour
            self.serviceTypeDescription = self.ruleMatrixData?.Data[index].ServiceTypeDescription
            self.serviceTypeDescriptionValue = self.ruleMatrixData?.Data[index].ServiceTypeDescriptionValue
            self.bookingServiceModeID = self.ruleMatrixData?.Data[index].BookingServiceModeID
            self.bookingModeDescriptionValue = self.ruleMatrixData?.Data[index].BookingModeDescriptionValue
            self.iSRequiredPickup = self.ruleMatrixData?.Data[index].ISRequiredPickup
            self.maxOpsCount = self.ruleMatrixData?.Data[index].MaxOpsCount
            self.opsPanaltyDays = self.ruleMatrixData?.Data[index].OpsPanaltyDays
            self.maxEditableCount = self.ruleMatrixData?.Data[index].MaxEditableCount
        }
       
    }
    
    func initProductsDropDown(){
        OIL_CHANGE.didSelect{(selectedText , index ,id) in
            self.selectedProduct = selectedText
            self.selectedProductID = id
        }
    }
    func setUpRequiredInputLabels(){
        // to add asterisk at the end of the label text , for reequired inpputs
          let labels:[UILabel] = [LBL_SERVICES,LBL_OIL_CHANGE,LBL_DATE,LBL_TIME]
          customUI.reQuiredInputLabel(value: EbookingConstants.requiredTxtUpdateBooking, label:labels)
      }

    @IBAction func UpdateBooking(_ sender: Any) {
        btnInputs.append(DATE)
        btnInputs.append(TIME)
        if(EbookingUtils.isAllBTNInputsValid(btns: btnInputs)){
            
            let date = DATE.titleLabel?.text! as String?
            let time = TIME.titleLabel?.text! as String?
            let param = ["BookingDetailsID":bookingData.BookingDetailsID,
                          "BookingDate":date!,
                          "BookingTime":time!,
                          "ProductID":"\(self.selectedProductID!)",
                          "ProductQuality":"\(bookingData.ProductQuality)",
                          "PromoCode":bookingData.PromoCode
                          ] as [String : Any]?
            print("param ---- ",param!)
// 21/10/2020 15 - 17 Shell Hellix  ST222S Sher Tantan
            
            // Shell rimula 17-10-2020
            EbookingUtils.showLoading(view: self.view)
            
            apiRequest.request(url:URL(string: EbookingConstants.Api.updateBooking)!,parameters: param!){responseCode,responseMessage,json,response in
                    if(responseCode == .Success){
                        let decoder = JSONDecoder()
                        do{
                            let responseData = try decoder.decode(AddNewBookingAPIResponse.self, from: response.data!)
                            self.newBookingID = responseData.Data
                            self.bookingMessage = responseMessage.message
                             print("ADDBOOKING json ---",json!)
                            self.showUpdateConfirmation(msg:responseMessage.message) // data
                         }catch{
                            EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_ADD_NEW_BOOKING,view: self.view)
                        }

                    }else{
                        // show popup instead
                        EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                    }
                EbookingUtils.stopLoading(view: self.view)
                }
        }
    }
    
    func showUpdateConfirmation(msg:String){
        PopUpBookingUpdate.instance.showPopUp(message: msg, nav: self.navigationController!)
    }
    
    
}
