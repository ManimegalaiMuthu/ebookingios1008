//
//  BookingDetailsViewController.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BookingDetailsViewController: UIViewController {
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    @IBOutlet weak var LBL_STATUS_DISPLAY: UILabel!
    @IBOutlet weak var LBL_VEHICLE_CUSTOMER: UILabel!
    @IBOutlet weak var LBL_DATE: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    @IBOutlet weak var LBL_SERVICE_TYPE: UILabel!
    @IBOutlet weak var LBL_PRODUCT: UILabel!
    @IBOutlet weak var LBL_PROMO_CODE: UILabel!
    var mobileNumber = ""
    @IBOutlet weak var BTN_ACTION_CLICK: UIButton!
    var bookingData : ResponseData!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.red
        EbookingUtils.setUpNavigationBar(navigationController: self.navigationController!, navigationItem: navigationItem, title: EbookingConstants.BOOKING_DETAILS)
        LBL_BOOKING_ID.text = bookingData.BookingDetailsID
        LBL_STATUS_DISPLAY.text = bookingData.StatusDisplay
        LBL_VEHICLE_CUSTOMER.text = bookingData.VehicleNumber +
            " (\(bookingData.ConsumerName))"
        LBL_DATE.text = bookingData.BookingDate
        LBL_TIME.text = bookingData.BookedFrom + " - " + bookingData.BookedTo
        LBL_SERVICE_TYPE.text = bookingData.ServiceTypeDescription
        LBL_PRODUCT.text = bookingData.ProductName
        LBL_PROMO_CODE.text = bookingData.PromoCode
        BTN_ACTION_CLICK.isHidden = true
        if(bookingData.Status == EbookingConstants.CONFIRMED_STATUS){
            BTN_ACTION_CLICK.isHidden = false
        }
        
    }
    
    @IBAction func CallBtn(_ sender: Any) {
        mobileNumber = "07768988959"
        let url:NSURL = URL(string: "TEL://\(mobileNumber)")! as NSURL
        UIApplication.shared.open(url as URL, options:[:], completionHandler: nil)

        
    }
    
    // CANCELL BOOKING
    @IBAction func BtnActionClick(_ sender: Any) {
        
        
    }
    

}
