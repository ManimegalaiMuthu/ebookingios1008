//
//  AcceptDeclineModifyViewController.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AcceptDeclineModifyViewController: UIViewController {

    @IBOutlet weak var LBL_STATUS_DISPLAY: UILabel!
    
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    @IBOutlet weak var LBL_VEHICLE_CUSTOMER: UILabel!
    @IBOutlet weak var LBL_DATE: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    @IBOutlet weak var LBL_SERVICE_TYPE: UILabel!
    @IBOutlet weak var LBL_PRODUCT: UILabel!
    @IBOutlet weak var LBL_PROMO_CODE: UILabel!
    var apiRequest = APIRequest()
    var bookingData : ResponseData!
    var bookingID:String?
    var mobileNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.red
        EbookingUtils.setUpNavigationBar(navigationController: self.navigationController!, navigationItem: navigationItem, title: EbookingConstants.BOOKING_DETAILS)
        LBL_BOOKING_ID.text = bookingData.BookingDetailsID
        LBL_STATUS_DISPLAY.text = bookingData.StatusDisplay
        LBL_VEHICLE_CUSTOMER.text = bookingData.VehicleNumber +
            " (\(bookingData.ConsumerName))"
        LBL_DATE.text = bookingData.BookingDate
        LBL_TIME.text = bookingData.BookedFrom + " - " + bookingData.BookedTo
        LBL_SERVICE_TYPE.text = bookingData.ServiceTypeDescription
        LBL_PRODUCT.text = bookingData.ProductName
        LBL_PROMO_CODE.text = bookingData.PromoCode
        
    }
    

    @IBAction func BTNCall(_ sender: Any) {
        mobileNumber = "07768988959"
        let url:NSURL = URL(string: "TEL://\(mobileNumber)")! as NSURL
        UIApplication.shared.open(url as URL, options:[:], completionHandler: nil)
        
    }
    @IBAction func AcceptBooking(_ sender: Any) {
        // BookingDetailsID BookingStatusID Reason Remarks
        let param = ["BookingDetailsID":bookingData.BookingDetailsID,
                     "BookingStatusID":EbookingConstants.CONFIRMED_STATUS,
                      "Reason":"",
                      "Remarks":""] as [String : Any]?
        EbookingUtils.showLoading(view: self.view)
        
        apiRequest.request(url:URL(string: EbookingConstants.Api.updateBookingStatus)!,parameters: param!){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                        self.bookingID = self.bookingData.BookingDetailsID
                        print("ADDBOOKING json ---",json!)
                        self.showAcceptedBooking(msg:responseMessage.message,id:self.bookingID!) // data
                }else{
                    // show popup instead
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
    }
    @IBAction func DeclineBooking(_ sender: Any) {
        PopUpDeclineModify.instance.showPopUp(nav: self.navigationController!,bookingResponse: bookingData)
    }
    
    
    func showAcceptedBooking(msg:String,id:String){
        PopUpAcceptedBooking.instance.showPopUp(message: msg, bookingId: id, nav: self.navigationController!)
        
    }

}
