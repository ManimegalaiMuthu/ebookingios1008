//
//  AddNewBookingController.swift
//  TestApp
//
//  Created by Admin on 8/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import iOSDropDown
import SwiftyJSON
import Alamofire

class AddNewBookingController: UIViewController {

    @IBOutlet weak var LBL_VEHICLE_ID: UILabel!
    @IBOutlet weak var LBL_SERVICE_TYPE: UILabel!
    @IBOutlet weak var LBL_MOBILE_NUMBER: UILabel!
    @IBOutlet weak var LBL_EMAIL_ADD: UILabel!
    @IBOutlet weak var LBL_PREPARED_OIL: UILabel!
    @IBOutlet weak var LBL_DATE: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    @IBOutlet weak var LBL_FIRST_NAME: UILabel!
    @IBOutlet weak var LBL_LAST_NAME: UILabel!
    
    @IBOutlet weak var TXT_VEHICLE_ID: UITextField!
    @IBOutlet weak var TXT_MOBILE_NUM: UITextField!
    @IBOutlet weak var TXT_FIRST_NAME: UITextField!
    @IBOutlet weak var TXT_LAST_NAME: UITextField!
    @IBOutlet weak var TXT_COUNTRY_CODE: UITextField!
    @IBOutlet weak var TXT_EMAIL: UITextField!
    
    @IBOutlet weak var TXT_PROMO_CODE: UITextField!
    @IBOutlet weak var BTN_SERVICE_TIME: UIButton!
    @IBOutlet weak var BTN_SERVICE_DATE: UIButton!
    @IBOutlet weak var OIL_CHANGE_PRODUCT_DROPDOWN: DropDown!
    @IBOutlet weak var MAJOR_MINOR_SERVICE_DROPDOWN: DropDown!
    
    
    @IBOutlet weak var DATE_PARENT_VIEW: UIView!
    @IBOutlet weak var TIME_PARENT_VIEW: UIView!
    @IBOutlet weak var OIL_CHANGE_PARENT_VIEW: UIView!
    @IBOutlet weak var MAJOR_MINOR_PARENT_VIEW: UIView!
    
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    var uiWidgets : CustomUIWidgets! = nil
    let dateTimeTableView = UITableView()
    var customUI: CustomUIWidgets! = nil
    let ebookingUtils = EbookingUtils()
    var currentButton = UIButton()
    var source:[String] = []
    var apiRequest = APIRequest()
    var productsArray = [String]()
    var serviceArray = [String]()
    var textFields = [UITextField]()
    var btnInputs = [UIButton]()
    var ruleMatrixData:GetRuleMatrixAPIResponse?
    
    var selectedProduct:String?
    var selectedProductID:Int?
    var newBookingID:String?
    var bookingMessage:String?
    
    var bookingMatrixID :Int?
    var bookingServiceTypeID :Int?
    var serviceTypeDurationHour :Int?
    var serviceTypeDescription :String?
    var serviceTypeDescriptionValue :String?
    var bookingServiceModeID :Int?
    var bookingModeDescriptionValue :String?
    var iSRequiredPickup :Bool?
    var maxOpsCount :Int?
    var opsPanaltyDays :Int?
    var maxEditableCount :Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        uiWidgets = CustomUIWidgets()
        TXT_COUNTRY_CODE.isEnabled = false
        EbookingUtils.setUpNavigationBar(navigationController: self.navigationController!, navigationItem: navigationItem, title: EbookingConstants.ADD_NEW_BOOKING)
        setUpRequiredInputLabels()
        getProducts()
        TXT_VEHICLE_ID.setBottomBorderOnlyWith(color: UIColor.gray.cgColor)
    }
    // TradeID
    func getProducts(){
        EbookingUtils.showLoading(view: self.view)
        let sampleTradeId = "T170000130"
        apiRequest.request(url:URL(string: EbookingConstants.Api.getProducts)!,parameters: ["TradeID":sampleTradeId]){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(GetProductsAPIResponse.self, from: response.data!)
                        let data = responseData.Data
                        let objects = data
                        self.productsArray = ShellProducts.getProductName(object: objects)
                        self.OIL_CHANGE_PRODUCT_DROPDOWN.optionArray = self.productsArray
                        self.OIL_CHANGE_PRODUCT_DROPDOWN.optionIds = ShellProducts.getProductID(object: objects)
                        self.getRulematrix()
                     }catch{
                        EbookingUtils.stopLoading(view: self.view)
                        EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_GET_PRODUCTS,view: self.view)
                    }
                }else{
                    EbookingUtils.stopLoading(view: self.view)
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            }
        }
    
    func getRulematrix(){
        apiRequest = APIRequest()
        apiRequest.request(url:URL(string: EbookingConstants.Api.getRuleMatrix)!,parameters: [:]){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(GetRuleMatrixAPIResponse.self, from: response.data!)
                        self.ruleMatrixData = responseData
                        self.serviceArray = RuleMatrix.getServiceTypeDescriptionValue(object: responseData)
                        self.MAJOR_MINOR_SERVICE_DROPDOWN.optionArray = self.serviceArray
                        self.initServiceTypeDropDown()
                        self.initProductsDropDown()
                     }catch{
                        EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_GET_SERVICES,view: self.view)
                    }
                }else{
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
        
    }
    
    func addNewBooking(param: Parameters = [:]){
        print("ADDBOOKING param ---",param)
        EbookingUtils.showLoading(view: self.view)
        apiRequest.request(url:URL(string: EbookingConstants.Api.addNewBooking)!,parameters: param){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(AddNewBookingAPIResponse.self, from: response.data!)
                        self.newBookingID = responseData.Data
                        self.bookingMessage = responseMessage.message
                        print("ADDBOOKING",responseData.Data! + " -- " + responseMessage.message)
                        print("ADDBOOKING responseData -- ", responseData)
                        print("ADDBOOKING json ---",json!)
                        self.showBookingConformation() // data
                     }catch{
                        EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_ADD_NEW_BOOKING,view: self.view)
                    }
                    
                }else{
                    // show popup instead
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
        
    }
    
    @IBAction func AddNewBooking(_ sender: Any) {
        textFields.append(TXT_VEHICLE_ID)
        textFields.append(MAJOR_MINOR_SERVICE_DROPDOWN)
        textFields.append(TXT_MOBILE_NUM)
        textFields.append(TXT_EMAIL)
        textFields.append(TXT_FIRST_NAME)
        textFields.append(TXT_LAST_NAME)
        textFields.append(OIL_CHANGE_PRODUCT_DROPDOWN)
        btnInputs.append(BTN_SERVICE_TIME)
        btnInputs.append(BTN_SERVICE_DATE)
        if(EbookingUtils.isAllInputsValid(textFields: textFields) && EbookingUtils.isAllBTNInputsValid(btns: btnInputs)){
            let date = BTN_SERVICE_DATE.titleLabel?.text! as String?
            let time = BTN_SERVICE_TIME.titleLabel?.text! as String?
            let param = ["MobileNumber":TXT_MOBILE_NUM.text!,
                         "VehicleNumber":TXT_VEHICLE_ID.text!,
                         "EmailAddress":TXT_EMAIL.text!,
                         "StreetName":"","BuildingName":"","UnitNumber":"","PostalCode":"",
                         "BookingMatrixID":"\(self.bookingMatrixID!)",
                         "BookingDate":date!,
                         "BookingTime":time!,
                         "ProductID":"\(self.selectedProductID!)",
                         "ProductQuality":"2",
                         "FirstName":TXT_FIRST_NAME.text!,
                         "LastName":TXT_LAST_NAME.text!,
                         "PromoCode":TXT_PROMO_CODE.text!] as [String : Any]?
                  self.addNewBooking(param: param!)
        }
    }
    
    @IBAction func showServiceDate(_ sender: AnyObject?) {
        ebookingUtils.showDatePicker(view: self.view, btn: BTN_SERVICE_DATE)
    }
    @IBAction func showServiceTime(_ sender: AnyObject?) {
        ebookingUtils.showTimePicker(view: self.view, btn: BTN_SERVICE_TIME)
    }
    func initServiceTypeDropDown(){
            MAJOR_MINOR_SERVICE_DROPDOWN.didSelect{(selectedText , index ,id) in
                self.bookingMatrixID = self.ruleMatrixData?.Data[index].BookingMatrixID
                self.bookingServiceTypeID = self.ruleMatrixData?.Data[index].BookingServiceTypeID
                self.serviceTypeDurationHour = self.ruleMatrixData?.Data[index].ServiceTypeDurationHour
                self.serviceTypeDescription = self.ruleMatrixData?.Data[index].ServiceTypeDescription
                self.serviceTypeDescriptionValue = self.ruleMatrixData?.Data[index].ServiceTypeDescriptionValue
                self.bookingServiceModeID = self.ruleMatrixData?.Data[index].BookingServiceModeID
                self.bookingModeDescriptionValue = self.ruleMatrixData?.Data[index].BookingModeDescriptionValue
                self.iSRequiredPickup = self.ruleMatrixData?.Data[index].ISRequiredPickup
                self.maxOpsCount = self.ruleMatrixData?.Data[index].MaxOpsCount
                self.opsPanaltyDays = self.ruleMatrixData?.Data[index].OpsPanaltyDays
                self.maxEditableCount = self.ruleMatrixData?.Data[index].MaxEditableCount
              //  print("Selected String: \(selectedText) \n bookingMatrixID: //\(self.bookingMatrixID)")
          }
      }
    func initProductsDropDown(){
        OIL_CHANGE_PRODUCT_DROPDOWN.didSelect{(selectedText , index ,id) in
            self.selectedProduct = selectedText
            self.selectedProductID = id
        }
        
    }
    func showBookingConformation(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "AddNewBooking", bundle: nil)
               let vController = storyBoard.instantiateViewController(withIdentifier: "BookingConfirmed") as! BookingConfirmedViewController
        vController.message = self.bookingMessage
        vController.bookingID = self.newBookingID
        print("ADDBOOKING",self.newBookingID! + " --xx-- " + self.bookingMessage!)
               self.navigationController?.pushViewController(vController, animated: true)
    }
   
    func setUpRequiredInputLabels(){
        let labels:[UILabel] = [LBL_VEHICLE_ID,LBL_SERVICE_TYPE,LBL_MOBILE_NUMBER,LBL_EMAIL_ADD,
                      LBL_PREPARED_OIL,LBL_DATE,LBL_TIME,LBL_FIRST_NAME,LBL_LAST_NAME]
        uiWidgets.reQuiredInputLabel(value: EbookingConstants.requiredTxtAddBooking, label:labels)
     
    }

}
