//
//  BookingConfirmedViewController.swift
//  TestApp
//
//  Created by Admin on 8/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BookingConfirmedViewController: UIViewController {

    @IBOutlet weak var LBL_MESSAGE: UILabel!
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    var message:String?
    var bookingID:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        LBL_MESSAGE.text = message
        LBL_BOOKING_ID.text = bookingID
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(self.backToInitial(sender:)))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.red
        
        // Assign back button with back arrow and image
//        navigationItem.leftBarButtonItems = CustomBackButton.createWithImage(UIImage(named: "yourImageName")!, color: UIColor.yourColor(), target: weakSelf, action: #selector(self.backToInitial(sender:)))

        
    }
    @objc func backToInitial(sender: AnyObject) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func BackToBookingList(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
