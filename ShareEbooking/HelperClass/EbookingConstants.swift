//
//  EbookingConstants.swift
//  TestApp
//
//  Created by Admin on 8/21/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import Alamofire

public struct EbookingConstants {
    
    struct StoryBoards {
        static let MAIN_STORYBOARD = "MainStoryBoard"
        static let ADD_NEW_BOOKING_STORYBOARD = "AddNewBooking"
        static let BOOKING_STATUS = "BookingStatus"
    }
    
    struct VControllerIdentifier{
        static let MAIN_NAVIGATION_ID = "MainNavigationController"
        static let ALL_BOOKING_LIST_ID = "AllBookingListViewController"
        static let CALENDAR_VIEW_ID = "CalendarViewController"
        static let ADD_NEW_BOOKING_ID = "AddNewBookingController"
        static let CONFIMED_BOOKING_ID = "BookingConfirmedViewController"
        static let UPDATE_BOOKING_ID = "UpdateBookingInfoViewController"
        static let ACCEPT_DECLINE_MODIFY_ID = "AcceptDeclineModifyViewController"
        static let BOOKING_DETAILS_ID = "BookingDetailsViewController"
    }
    
    struct PopUpIdentifier{
        static let POP_ACCEPTED_BOOKING = "popup_accepted_booking"
        static let POP_DECLINE_MODIFY_BOOKING = "popup_decline_modify"
        static let POP_DECLINE_REASON = "popup_decline_reason"
        static let POP_COMMON = "popup_common"
        
    }

    
    public static let PENDING_WORKSHOP_STATUS: Int = 0
    public static let PENDING_CUSTOMER_STATUS: Int = 1
    public static let CONFIRMED_STATUS: Int  = 2
    public static let CANCELLED_STATUS: Int = 8
    public static let DECLINED_STATUS: Int = 4
    public static let SERVICE_STARTS_STATUS: Int = 16
    public static let IN_PROGRESS_STATUS: Int = 32
    public static let SERVICE_ENDS_STATUS: Int = 64
    public static let NO_SHOW_STATUS: Int = 128
    public static let COMPLETED_STATUS: Int = 256

    public static let PENDING_WORKSHOP: String = "Pending Confirmation(Workshop)"
    public static let PENDING_CUSTOMER: String = "Pending Confirmation(Customer)"
    public static let CONFIRMED: String = "Confirmed"
    public static let CANCELLED: String  = "Cancelled"
    public static let DECLINED: String  = "Declined"
    public static let SERVICE_STARTS: String  = "Service Starts"
    public static let IN_PROGRESS: String  = "In Progress"
    public static let SERVICE_ENDS: String  = "Service Ends"
    public static let NO_SHOW: String = "No Show"
    public static let COMPLETED: String = "Completed"

    
     static let ADD_NEW_BOOKING = "ADD NEW BOOKING"
     static let MY_BOOKINGS = "MY BOOKINGS"
     static let BOOKING_DETAILS = "BOOKING DETAILS"
     static let EDIT_BOOKING = "EDIT BOOKING"
    
    static let NO_RESULT_FOUND = "No results found."
    static let FAILED_TO_GET_BOOKINGS = "Failed to get bookings."
    static let FAILED_TO_ADD_NEW_BOOKING = "Failed to Add new booking."
    static let FAILED_TO_GET_PRODUCTS = "Failed to get products."
    static let FAILED_TO_GET_SERVICES = "Failed to get services."
    
    public static let VEHICLE_ID: String = "Vehicle ID"
    public static let MINOR_MAJOR_SERVICE = "Minor / Major Service"
    public static let MOBILE_NUMBER = "Mobile Number"
    public static let EMAIL_ADD = "EmailL Adress"
    public static let PREF_OIL_CHANGE_PROD = "Preferred Oil Change Product"
    public static let DATE = "Date"
    public static let TIME = "Time"
    public static let FIRST_NAME = "First Name"
    public static let LAST_NAME = "Last Name"
    
    static let OTHER_REASON = "Others: Please specify reason."
    static let OUT_OF_STOCK = "Product selected is out of stock"
    static let UNAVAILABLE_SLOT = "Unavailable slot"
    
    public static let requiredTxtAddBooking = [VEHICLE_ID,MINOR_MAJOR_SERVICE,MOBILE_NUMBER,
                        EMAIL_ADD,PREF_OIL_CHANGE_PROD,DATE,TIME,FIRST_NAME,LAST_NAME]
    
    public static let requiredTxtUpdateBooking = ["Oil Change Service Type","Preferred Oil Change Product","Date","Time"]
       
   struct Api {
       static let base = "http://shell-stg.ersg-staging.com/services/"
       static let fetchBookingUrl = base + "API/Booking/FetchBookingList"
       static let getProducts = base + "API/Trade/LoadProduct"
       static let getRuleMatrix = base + "API/Booking/FetchBookingRuleMatrix"
       static let addNewBooking = base + "API/Booking/AddNewBooking"
       static let updateBooking = base + "API/Booking/ModifyBooking"
       static let updateBookingStatus = base + "API/Booking/UpdateBookingStatus"
   
    }
    
    public struct UserDefault {
       public static let ERToken:String = "ERToken"
       public static let LanguageCode:String = "LanguageCode"
       public static let AppVersion:String = "AppVersion"
       public static let AppID:String = "AppID"
       public static let AuthToken:String = "AuthToken"
       public static let CountryCode:String = "CountryCode"
       public static let UserID:String = "UserID"
       public static let PushToken:String = "PushToken"
     }
     ///
    
    
    public struct Headers {
      
      public static func PreLogin() -> HTTPHeaders {
        return [
          "ER-TOKEN":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.ERToken) ?? "",
          "Content-Type":"application/json",
          "LanguageCode":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) ?? "",
          "AppVersion":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.AppVersion) ?? "",
          "APPID":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.AppID) ?? "",
          "CountryCode":"360"
        ]
      }
      
      public static func PostLogin() -> HTTPHeaders {
        return [
          "ER-TOKEN":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.ERToken) ?? "",
          "Content-Type":"application/json",
          "LanguageCode":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) ?? "",
          "AppVersion":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.AppVersion) ?? "",
          "APPID":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.AppID) ?? "",
          "AUTH-TOKEN":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.AuthToken) ?? "",
          "CountryCode":"360",
          "TradeID":UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.UserID) ?? ""
        ]
      }
        
        public static func apiHeader() -> HTTPHeaders {
          return [
           "ER-TOKEN":"SHARE-297992CD-74CE-415C-B7D1-B1805A79F398",
            "Content-Type":"application/json",
            "LanguageCode":"5",
            "AppVersion":"tp1.0.13",
            "APPID":"20170727",
            "AUTH-TOKEN":"c634ba71-8968-4f31-8ef6-66dc5df0c651",//df7efa16-ba36-4832-ba99-6d0508a1dcf7",
            "CountryCode":"360",
            "TradeID":"T170000130",
            "ProgramID":"1"
          ]
        }
      
    }

    
    
    ////
}
